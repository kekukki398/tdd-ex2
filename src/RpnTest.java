import static org.junit.Assert.*;

import java.util.Stack;

import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;


public class RpnTest {

	private RpnCalc rpn = new RpnCalc();
	
	@Test
	public void newCalculatorHasZeroInAccumulator() {
		assertThat(rpn.getAccumulator(), is(0));
	}
	
	@Test
	public void accumulatorCanBeSet() {
		rpn.setAccumulator(1);
		assertThat(rpn.getAccumulator(), is(1));
	}

	@Test
	public void CalculatorSupportsAddition() {
		rpn.setAccumulator(1);
        rpn.enter();
        rpn.setAccumulator(2);
        rpn.plus();
        assertThat(rpn.getAccumulator(), is(3));
	}
	
	@Test
	public void CalculatorSupportsMultiplication() {
		rpn.setAccumulator(1);
        rpn.enter();
        rpn.setAccumulator(2);
        rpn.plus();
        rpn.enter();
        rpn.setAccumulator(4);
        rpn.multiply();
        assertThat(rpn.getAccumulator(), is(12));
	}
	
	@Test
	public void CalculatorSupportsMoreComplexMultiplication() {
		rpn.setAccumulator(4);
        rpn.enter();
        rpn.setAccumulator(3);
        rpn.plus();
        rpn.enter();

        rpn.setAccumulator(2);
        rpn.enter();
        rpn.setAccumulator(1);
        rpn.plus();
        
        rpn.multiply();
        assertThat(rpn.getAccumulator(), is(21));
	}
	
	@Test
	public void CalculatorSupportsStringEvaliation() {
		rpn.evaluate("5 1 2 + 4 * + 3 +");
        assertThat(rpn.getAccumulator(), is(20));
        rpn.evaluate("5 1 + 7 * 6 3 + +");
        assertThat(rpn.getAccumulator(), is(51));
	}
}
class RpnCalc{

	private int accumulator;
	Stack<Integer> stack = new Stack<>();

	public int getAccumulator() {
		return accumulator;
	}

	public void evaluate(String string) {
		for (String token: string.split(" ")) {
			if ("+".equals(token)) {
				stack.pop();
        		plus();
        		enter();
        	} else if ("*".equals(token)) {
        		stack.pop();
        		multiply();
        		enter();
        	} else {
        		setAccumulator(Integer.parseInt(token));
        		enter();
        	}
		}	
	}

	public void multiply() {
		setAccumulator(stack.pop() * getAccumulator());
	}

	public void plus() {
		setAccumulator(stack.pop() + getAccumulator());
	}

	public void enter() {
		stack.push(accumulator);
	}

	public void setAccumulator(int accumulator) {
		this.accumulator = accumulator;
	}
	
}
